import React from "react";
import Cookies from "universal-cookie";

const cookies = new Cookies();

const AuthorizedDefaultValue = {
  isLoggedIn: false,
  userLevel: "",
  setAuthorizedValue: () => {},
};

const AuthorizedContext = React.createContext(AuthorizedDefaultValue);

const AuthorizedContextProvider = (props) => {
  const [isLoggedIn, setIsLoggedIn] = React.useState(false);
  const [userLevel, setUserLevel] = React.useState("");
  const accessToken = cookies.get("accessToken");


  const setAuthorizedValue = React.useCallback(
    (loginStatus, userLevelStatus) => {
      setIsLoggedIn(loginStatus);
      setUserLevel(userLevelStatus);
    },
    [setIsLoggedIn, setUserLevel]
  );


  React.useEffect(() => {
    if (accessToken) {
      setAuthorizedValue(true);
    }
  }, [accessToken, setAuthorizedValue]);

  return (
    <AuthorizedContext.Provider
      value={{ isLoggedIn, userLevel, setAuthorizedValue }}
    >
      {props.children}
    </AuthorizedContext.Provider>
  );
};

const useAuthorizedContext = () => {
  const { isLoggedIn, userLevel, setAuthorizedValue } =
    React.useContext(AuthorizedContext);

  return { isLoggedIn, userLevel, setAuthorizedValue };
};

export { AuthorizedContextProvider, useAuthorizedContext };
