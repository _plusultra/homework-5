import React, { useCallback } from "react";
import { Row, Col, Form, Button, Card, Spin, Space, Typography } from "antd";
import "./Home.css";
import NavbarComponent from "../../assets/components/navbar/NavbarComponent";
import { useAuthorizedContext } from "../../AuthorizedContext";
import useGetTransaction from "../../Query/useGetTransaction";
import moment from "moment";
import useDeleteTransaction from "../../Mutations/useDeleteTransaction";

const { Text } = Typography;

const CardTransactionComponent = (props) => {
  const { mutate: deleteTransaction } = useDeleteTransaction(
    props.transaction.id,
    props.refetchTransactions
  );

  const handleCancelTransaction = useCallback(() => {
    console.log("id transaction >> ", props.transaction.id);
    deleteTransaction();
  }, [props.transaction.id, deleteTransaction]);

  return (
    <Card title=" " style={{ width: "100%" }}>
      <Form style={{ marginLeft: "10%" }}>
        <Row>
          <Text>Id transaksi : {props.transaction.id}</Text>
        </Row>
        <Row>
          <Col style={{ width: "35%" }}>
            <p>Waktu Request </p>
          </Col>
          <Col offset={0} style={{ width: "65%" }}>
            <p>
              {" "}
              :{" "}
              {moment(new Date(props.transaction.created_date)).format(
                "DD MMMM YYYY"
              )}
            </p>
          </Col>
        </Row>
        <Row>
          <Col style={{ width: "35%" }}>
            <p>Jenis Transaksi</p>
          </Col>
          <Col style={{ width: "65%" }}>
            <p>: {props.transaction.jenis_transaksi} </p>
          </Col>
        </Row>

        <Row>
          <Col style={{ width: "35%" }}>
            <p>Nominal Transaksi</p>
          </Col>
          <Col sstyle={{ width: "65%" }}>
            <p> : {props.transaction.nominal_transaksi} </p>
          </Col>
        </Row>

        <Row>
          <Col style={{ width: "35%" }}>
            <p>Alamat Agen</p>
          </Col>
          <Col style={{ width: "65%" }}>
            <p> : {props.transaction.address} </p>
          </Col>
        </Row>

        <Row>
          <Col style={{ width: "35%" }}>
            <p>Status</p>
          </Col>
          <Col style={{ width: "65%" }}>
            <p>: {props.transaction.status}</p>
          </Col>
        </Row>

        <div className="float-right">
          <Button
            type="primary"
            style={{
              margin: "0px",
              paddingRight: "15px",
              backgroundColor: "#F03D3E",
              fontWeight: "bold",
            }}
            onClick={handleCancelTransaction}
          >
            Batalkan
          </Button>
        </div>
      </Form>
    </Card>
  );
};

function Home() {
  const { isLoggedIn, userLevel } = useAuthorizedContext();
  console.log("value >> ", isLoggedIn, userLevel);
  const { data, isError, isLoading, refetch: refetchTransactions } = useGetTransaction();
  console.log("data >> ", isLoading, data);

  React.useEffect(() => {
    //setState loading to true
    //do fetching
    //setState loading to false
    //setState response data
  }, []);

  return (
    <div className="outer-home">
      <NavbarComponent />

      <div className="statusTransaksi">
        <div className="title">
          <p>Transaksi Saat Ini:</p>
        </div>
        <div className="resume">
          <Space direction="vertical">
            {isLoading ? (
              <Spin tip="Loading..."></Spin>
            ) : data ? (
              data.map((transaction) => (
                <CardTransactionComponent
                  key={transaction.id}
                  transaction={transaction}
                  refetchTransactions={refetchTransactions}
                />
              ))
            ) : (
              <Text>Gagal memuat data</Text>
            )}
          </Space>
        </div>
      </div>
    </div>
  );
}

export default Home;
