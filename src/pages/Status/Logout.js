import React from "react";
import { Button } from "antd";
import NavbarComponent from "../../assets/components/navbar/NavbarComponent";
import "../Status/logout.css";
import Cookies from "universal-cookie";

const cookies = new Cookies();

function Logout() {
  const handleClickButtonLogout = React.useCallback(() => {
    cookies.remove("accessToken");
  }, []);
  return (
    <div>
      <NavbarComponent />
      <div className="logout">
        <Button onClick={handleClickButtonLogout}>Keluar</Button>
      </div>
    </div>
  );
}

export default Logout;
